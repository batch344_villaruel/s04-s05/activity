import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws Exception {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact();
        contact1.setName("John Doe");
        contact1.getContactNumbers().add("+639152468596");
        contact1.getContactNumbers().add("+639228547963");
        contact1.getAddresses().add("my home in Quezon City");
        contact1.getAddresses().add("my office in Makati City");

        ArrayList<String> forJaneContact = new ArrayList<>();
        forJaneContact.add("+639162148573");
        forJaneContact.add("+639173698541");
        ArrayList<String> forJaneAddress = new ArrayList<>();
        forJaneAddress.add("my home in Caloocan City");
        forJaneAddress.add("my office in Pasay City");

        Contact contact2 = new Contact("Jane Doe", forJaneContact, forJaneAddress);

        phonebook.setContact(contact1);
        phonebook.setContact(contact2);
        if(phonebook.getContacts().isEmpty()){
            System.out.println("Phonebook is Empty");
        }else {


            for (Contact contact : phonebook.getContacts()) {
                System.out.println(contact.getName());
                System.out.println("--------------------");
                System.out.println(contact.getName() + " has the following registered numbers");
                for (String number : contact.getContactNumbers()) {
                    System.out.println(number);
                }
                System.out.println("------------------------------------");
                System.out.println(contact.getName() + " has the following registered addresses");
                for (String address : contact.getAddresses()) {
                    System.out.println(address);
                }
                System.out.println("============================================");
            }
        }

    }

}
