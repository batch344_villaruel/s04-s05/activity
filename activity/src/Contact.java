import java.util.ArrayList;

public class Contact {

    private String name;
    private ArrayList<String> contactNumbers;
    private ArrayList<String> addresses;

    public Contact(){
        this.name = "";
        this.contactNumbers = new ArrayList<>();
        this.addresses = new ArrayList<>();
    }
    public Contact(String name, ArrayList<String> contactNumbers, ArrayList<String> addresses){
        this.name = name;
        this.contactNumbers = contactNumbers;
        this.addresses = addresses;
    }

    public String getName(){
        return name;
    }
    public ArrayList<String> getContactNumbers(){
        return 	contactNumbers ;   //returns a copy of the arraylist
    }
    public ArrayList<String> getAddresses(){
        return addresses;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setContactNumbers(ArrayList<String> contactNumbers){
        this.contactNumbers = contactNumbers;
    }
    public void setAddresses(ArrayList<String> addresses){
        this.addresses = addresses;
    }

}
